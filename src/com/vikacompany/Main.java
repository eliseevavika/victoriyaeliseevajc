package com.vikacompany;

import com.vika.bank.Bank;
import com.vika.bank.Credit;

import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * @version 1.0
 * @author Eliseeva Vika
 */

public class Main {

    public static void main(String[] args) {

        boolean isDataCorrect = false;
        while (!isDataCorrect) {
            Scanner in = new Scanner(System.in);
            System.out.println("Введите сумму кредита:");
            int sum = in.nextInt();

            System.out.println("Введите срок кредита в месяцах:");
            int monthCount = in.nextInt();

            Bank bank = new Bank();
            //здесь вы можете добавить свои кредиты
            List<Credit> filteredCredits = bank.filterCredits(sum, monthCount);

            if (filteredCredits.size() > 0) {
                isDataCorrect = true;
                System.out.println(filteredCredits);
                System.out.println();
                System.out.println("Наиболее выгодный для вас вариант кредита");
                System.out.println(bank.getBestCredit(filteredCredits));
                boolean isNumberCorrect=false;
                int number = 0;
                while (!isNumberCorrect) {

                    System.out.println("Выберете номер кредита из представленного списка");
                    number = in.nextInt();

                    final int finalNumber = number;
                    List<Credit> collect = filteredCredits.stream()
                            .filter(c -> c.number == finalNumber)
                            .collect(Collectors.toList());

                    if(!collect.isEmpty()){
                        isNumberCorrect = true;
                    }else {
                        System.out.println("Введенный номер кредита не подходит под ваши условия");
                    }
                }
                try {
                    Credit credit = bank.getByNumber(number);
                    credit.printAnnuityPayment(sum, monthCount);
                    credit.printDifferentialPayment(sum, monthCount);
                } catch (Exception e) {
                    System.out.println(e);
                }
            } else {
                System.out.println("У банка нет подходящих кредитов");
            }
        }
    }
}

