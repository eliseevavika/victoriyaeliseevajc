package com.vika.bank;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *Класс банк
 * Содержит в себе список из 6 кредитов с указанными параметрами:
 * номером кредита, минимальной и максимальной суммой по кредиту, минимальным и максимальным сроком по кредиту, процентной ставкой.
 * Также проиводятся соответствующие фильтрации по кредитам, в зависимости от данных введенных пользователем
 */

public class Bank  {

    List<Credit> credits = new ArrayList<>();

    public Bank() {
        credits.add(new Credit("Потребительский кредит",15000, 1500000, 3, 60, 14.9));
        credits.add(new Credit("Ипотека с гос.поддержкой",300000, 1700000, 3, 360, 13.25));
        credits.add(new Credit("Ипотека на строящиеся дома",300000, 2000000, 12, 360, 12.75));
        credits.add(new Credit("Ипотека на новостройки",300000, 2000000, 12, 360, 12.25));
        credits.add(new Credit("Ипотека на строительство жилого дома",300000, 1750000, 3, 360, 12.75));
        credits.add(new Credit("Ипотека за загородную недвижимость",300000, 3000000, 12, 240, 11.4));
    }

    /**
     * Добавление кредита
     * @param credit экземпляр кредита
     */
    public void addCredit(Credit credit){
        credits.add(credit);
    }

    /**
     * Фильтрация кредитов по оптимальным условиям, представленных банком и введенных данных пользователем
     * @param sum  сумма кредита
     * @param monthCount срок кредита в месяцах
     * @return возвращает список кредитов, которые может предоставить банк, в зависимости от введенных пользовательских параметров
     */
    public List<Credit> filterCredits(int sum, int monthCount) {
        return credits.stream()
                .filter(c -> c.minSum <= sum && c.maxSum >= sum && c.minMonth <= monthCount && c.maxMonth >= monthCount)
                .collect(Collectors.toList());
    }

    /**
     * Фильтрация кредитов по наименьшей процентной ставке
     * @param filteredCredits кредиты, отфильтрованные в зависимости от введенных пользовательских параметров
     * @return возвращает кредит с наименьшей процентной ставкой
     */
    public Credit getBestCredit(List<Credit> filteredCredits) {
        return filteredCredits.stream()
                .min((c1, c2) -> Double.compare(c1.percent, c2.percent))
                .get();
    }

    /**
     * Фильтрация списка кредитов по номеру
     * @param number номер кредита
     * @return возврат первого элемента по номеру
     * @throws Exception обработка Исключения, если кредита с таким номером не существует в банке
     */
    public Credit getByNumber(int number) throws Exception {
        List<Credit> collect = credits.stream()
                .filter(c -> c.number == number)
                .collect(Collectors.toList());

        if (collect.isEmpty())
            throw new Exception("Не найден кредит с таким номером");

        return collect.get(0);
    }
}



