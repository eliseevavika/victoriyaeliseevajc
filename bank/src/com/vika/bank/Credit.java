package com.vika.bank;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Класс Кредит
 * Содержит в себе параметры кредита, также здесь производится расчет кредита по Аннуитентной и Дифференциальной схемам.
 */

public class Credit {

    private static final AtomicInteger count = new AtomicInteger(0);

    public final int number;
    public final int minSum;
    public final int maxSum;
    public final int minMonth;
    public final int maxMonth;
    public final double percent;
    private final String name;

    /**
     * @param minSum минимальная сумма по кредиту
     * @param maxSum максимальная сумма по кредиту
     * @param minMonth минимальное кол-во месяцев, на которое можно получить кредит
     * @param maxMonth максимальное кол-во месяцев, на которое можно получить кредит
     * @param percent процентная ставка
     */
    public Credit(String name,int minSum, int maxSum, int minMonth, int maxMonth, double percent) {
        this.number =  count.incrementAndGet();
        this.name = name;
        this.minSum = minSum;
        this.maxSum = maxSum;
        this.minMonth = minMonth;
        this.maxMonth = maxMonth;
        this.percent = percent;
    }


    @Override
    public String toString() {
        return "№ " +number+ "-"+name+" , процентная ставка по кредиту "+percent;
    }
    /**
     * Расчет и вывод аннуитентного платежа
     * <p>
     * Аннуитетный платеж – вариант ежемесячного платежа по кредиту, когда размер ежемесячного платежа остаётся постоянным на всём периоде кредитования.
     * </p>
     * @param s          сумма кредита
     * @param monthCount кол-во месяцев
     */

    public void printAnnuityPayment(int s, int monthCount) {
        double result = getAnnuityPayment(s, monthCount);
        System.out.println("Аннуитентная схема: сумма месячного платежа:");
        System.out.println(Math.ceil(result));
        System.out.println("Деталлизация расчета:");
        double mainDebtresult = 0.0;
        double creditBalance = s;
        for (int i = 0; i < monthCount; i++) {
            System.out.println(i + 1 + "-месяц: ");

            System.out.print("Остаток кредита:");
            creditBalance = creditBalance - mainDebtresult;
            System.out.println(Math.ceil(creditBalance));
            System.out.print("Проценты:");
            double percentDetal = creditBalance * this.percent / (12 * 100);
            System.out.println(Math.ceil(percentDetal));

            System.out.print("Основной долг:");
            mainDebtresult = result - percentDetal;
            System.out.println(Math.ceil(mainDebtresult));
            System.out.println();

        }
    }

    public double getAnnuityPayment(int s, int monthCount){
        double p = this.percent / (12 * 100);
        return Math.ceil(s * (p + p / (Math.pow(1 + p, monthCount) - 1)));
    }

    /**
     * Расчет и вывод диференцированного платежа
     * <p>
     * Дифференцированный платеж – вариант ежемесячного платежа по кредиту, когда размер ежемесячного платежа по погашению кредита постепенно уменьшается к концу периода кредитования.
     * </p>
     * @param s          сумма кредита
     * @param monthCount кол-во месяцев
     */
    public void printDifferentialPayment(int s, int monthCount) {
        List<Double> differentialPayment = getDifferentialPayment(s, monthCount);

        System.out.println("Дифференцированная схема:");
        System.out.println("Размер выплаты:");

        for (Double payment : differentialPayment) {
            System.out.println(Math.ceil(payment));

        }
    }

    public List<Double> getDifferentialPayment(int s, int monthCount) {
        double p = this.percent / (12 * 100);
        double principalDiff = (double) s / monthCount; // основной платеж

        List<Double> result = new ArrayList<>();
        for (int i = 0; i < monthCount; i++) {
            result.add(Math.ceil(principalDiff + (s - principalDiff * i) * p));
        }
        return result;
    }
}